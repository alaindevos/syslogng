(import simple-loops)
(import postgresql)
(define conn
	(connect '((host . "127.0.0.1")
                     (user . "x")
                     (password . "x")
                     (dbname . "syslogng"))
             );connect
    );define
(define selstring
	"select datetime,program,pid,message from messages_gentoo_20230926 order by datetime desc"
    );define
(define q
  (query conn selstring)
  );define 4

(define printrow
  (lambda (x)
    {begin
            (write x)
            (newline)
            };begin
            );lambda
  );define
(row-for-each
  printrow
  q
  );for
